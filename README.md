# Paragraphs anchor

A minimal addition to the popular paragraphs module that introduces an anchor tag field.

## How to use
- After installing the module, you will see a new anchor tag field on all paragraph types
- The vanilla textfield widget allows you to specify the anchor tag
- The HTML-safe widget allows you to source another plaintext field to generate an anchor tag

## Caveats
- Switching the source field in the form display settings WILL NOT update the anchor tags on existing paragraphs as these values are generated on save.

<?php

namespace Drupal\paragraphs_anchor\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\Entity\FieldConfig;

/**
 * Plugin implementation of the 'paragraphs_anchor' widget.
 *
 * @FieldWidget(
 *   id = "paragraphs_anchor_field_as",
 *   module = "paragraphs_anchor",
 *   label = @Translation("HTML-safe field output"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class FieldAsAnchorWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $field_options = array_keys(self::getAvailableFields());
    return [
      'source' => reset($field_options),
      'display' => TRUE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = array_merge($element, [
      '#type' => 'label',
      '#suffix' => $this->t('The anchor tag will be populated by the value in @field', ['@field' => $this->getSetting('source')]),
      '#access' => TRUE,
      '#element_validate' => [
        [static::class, 'validate'],
      ],
      '#value' => '[' . $this->getSetting('source') . ']',
    ]);

    if ($this->getSetting('display') == FALSE) {
      $element['#access'] = FALSE;
    }

    return ['value' => $element];
  }

  /**
   * Replace the value with source field placeholder.
   */
  public static function validate($element, FormStateInterface $form_state) {
    $value = $element['#value'];
    $form_state->setValueForElement($element, $value);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $field_options = self::getAvailableFields();

    $element['source'] = [
      '#type' => 'select',
      '#title' => $this->t('Source'),
      '#options' => $field_options,
      '#required' => TRUE,
      '#default_value' => $this->getSetting('source'),
    ];

    $element['display'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display anchor tag source'),
      '#default_value' => $this->getSetting('display'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $summary[] = $this->t('Source: @field - @display notice', [
      '@field' => $this->getSetting('source'),
      '@display' => $this->getSetting('display') ? 'with' : 'no',
    ]);

    return $summary;
  }

  /**
   * Helper function that returns all string type fields.
   *
   * @return array
   *   Return machine name to label array of string fields.
   */
  private static function getAvailableFields() {
    $route_parameters = \Drupal::routeMatch()->getParameters()->all();
    $field_options = [];

    if (isset($route_parameters['paragraphs_type'])) {
      $entity_field_manager = \Drupal::service('entity_field.manager');
      $field_definitions = $entity_field_manager->getFieldDefinitions('paragraph', $route_parameters['bundle']);
      foreach ($field_definitions as $name => $field_definition) {
        if ($field_definition instanceof FieldConfig && $field_definition->getType() == 'string') {
          $field_options[$name] = $field_definition->getLabel();
        }
      }
    }
    return $field_options;
  }

}
